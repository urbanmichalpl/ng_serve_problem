import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../clients.service';
import { Angular2Apollo } from 'angular2-apollo';
import gql from 'graphql-tag';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
})
export class ClientsComponent implements OnInit {

  clients: string;
  loading: boolean;

  // getClients(): void {
  //   this.clients = this.clientsService.getClients();
  // }

  // constructor(private clientsService: ClientsService) { }
  constructor(private apollo: Angular2Apollo) { }

  ngOnInit() {
    // this.getClients();
    this.apollo.watchQuery({
      query: gql`
        { users }
      `
    }).subscribe(({loading, data}) => {
      this.loading = loading;
      this.clients = data.users;
    });
  }
}
