import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '@angular/material';

import { AppComponent } from './app.component';
import { ClientsComponent } from './clients/clients.component';
import { NavigationSidebarComponent } from './navigation-sidebar/navigation-sidebar.component';
import { BookingsComponent } from './bookings/bookings.component';

import { ClientsService } from './clients.service';

import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloModule } from 'angular2-apollo';

const apolloClient = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: 'http://localhost:4000/graphql'
  })
});

const appRoutes: Routes = [
  { path: 'clients', component: ClientsComponent },
  { path: 'bookings', component: BookingsComponent },
  { path: '', component: ClientsComponent },
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ClientsComponent,
    NavigationSidebarComponent,
    BookingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    // TODO: Should I call forRoot and if yes should I provide appRoutes argumet (I'm calling forRoot already line below but for RouterModule)
    MaterialModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    ApolloModule.withClient(apolloClient)
  ],
  providers: [ClientsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
