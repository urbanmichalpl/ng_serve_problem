var express = require('express');
var bodyParser = require('body-parser');
var { apolloExpress, graphiqlExpress } = require('apollo-server');
var { makeExecutableSchema } = require('graphql-tools');
var cors = require('cors');

var typeDefs = [`
type Query {
  users: [String]
}

schema {
  query: Query
}`];

var resolvers = {
  Query: {
    users(root) {
      // var users =
      // [
      //   { firstName: "Michał", lastName: "Urban"},
      //   { firstName: "Justyna", lastName: "Młynarczuk"},
      // ];

      return ["Michał Urban", "Justyna Młynarczuk"]
    }
  }
};

//FIXES CORS ERROR
var originWhitelist = [
    'http://localhost:4200',
];

var corsOptions = {
    origin: function(origin, callback){
        var originIsWhitelisted = originWhitelist.indexOf(origin) !== -1;
        callback(null, originIsWhitelisted);
    },
    credentials: true
};

var schema = makeExecutableSchema({typeDefs, resolvers});
var app = express();
app.use(cors(corsOptions));
app.use('/graphql', bodyParser.json(), apolloExpress({schema}));
app.use('/graphiql', graphiqlExpress({endpointURL: '/graphql'}));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphiql'));
